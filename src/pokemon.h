#ifndef POKEMON_H
#define POKEMON_H

#include "utils.h"

class pokemon {
	private:
		char * name;
		char * type;
		char * attack_desc;
		char * weakness;
		int numCandy;
	public:
		pokemon();
		~pokemon();
		void add_p();
		void del_p();
		void update_p();
		void display();
		char * display_name();
		char * get_type();
};

#endif
