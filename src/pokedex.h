#ifndef POKEDEX_H
#define POKEDEX_H

#include "pokemon.h"
#include "utils.h"

/* Initiates Pokedex class, a dynamic array of pokemon objects */
class Pokedex {
	private:	
		pokemon * list;
		int num_pokemon;
		int size;
	public:
		Pokedex(int);
		Pokedex();
		~Pokedex();
		void read_list();
		void search_type();
		void display_one();
		void delete_pokemon();
		void display_list();
		void getChoice (char &);
};

#endif
