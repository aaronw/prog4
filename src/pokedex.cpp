#include "pokedex.h"
#include "pokemon.h"
#include "printFunc.h"
#include "utils.h"

/* The implementation for the Pokedex class.
 *
 * Data Members:
 * pokemon * list
 * int num_pokemon
 * int size;
 *
 */

/* Constructor for class Pokedex */
Pokedex::Pokedex(int size)
{
	list = new pokemon[size];
	num_pokemon = 0;
}

/* Constructor for class Pokedex */
Pokedex::Pokedex()
{
	char prompt[MAX_CHAR]{"How many entries are you making in your Pokedex?\n>> "};

	readInput(prompt, size);
	list = new pokemon[size];
	num_pokemon = 0;
}

/* Deconstructor for class Pokedex, assigs nil values to all data members */
Pokedex::~Pokedex()
{
	if (list)
		delete [] list; // - will implicitly call destructor on every pokemon object in array
	list = 0;
	size = 0;
	num_pokemon = 0;
}

/* Read in pocket monsters until user quits or fills list */
void Pokedex::read_list()
{
	char choice = ' ',
	     prompt[MAX_CHAR]{"Enter another?\n>> "};

	if (num_pokemon == size)
	{
		cout << "Current memory at MAX. You must restart program and add a larger entry value."; 
		return;
	}

	do
	{
		if (!size) return;
		list[num_pokemon].add_p();
		++num_pokemon;
		cout << endl;
		lineBreak();
		if (num_pokemon < size)
			readInput(prompt, choice);
	} while ('y' == tolower(choice) && num_pokemon < size);
}

/* Display all pocket monsters in the list */
void Pokedex::display_list()
{
	resetView();
	if (!num_pokemon)
	{
		cout << "No Pokémon saved!\n\n";
		return;
	}
	else 
	{
		cout << "\n\nPOKEMON COLLECTION" << endl;
		lineBreak();
		for (int i = 0; i < num_pokemon; ++i)
		{
			list[i].display();
			lineBreak();
		}
	}
}

/* Function: search_type()
   Desc: Prompts user to enter search value, searches for pocket monster by type */
void Pokedex::search_type()
{
	char prompt[MAX_CHAR]{"\nEnter 'type' of Pokémon: "},
	     typeBuf[MAX_CHAR]{" "};

	resetView();
	if (!num_pokemon)
	{
		cout << "No Pokémon saved!!\n\n";
		return;
	}
	else
	{
		readInput(prompt, typeBuf);
		for (int i = 0; i < num_pokemon; ++i)
		{
			cout << "\n\n";
			lineBreak();
			if (strcmp(typeBuf, list[i].get_type()) == 0)
				list[i].display();
		}
	}
}


/* Display one pocket monster's name */
void Pokedex::display_one()
{
	for (int i = 0; i < num_pokemon; ++i)
	{
		cout << i + 1 << ") " << list[i].display_name() << endl;
	}

}

/* Function: getChoice
 * Desc: prompts user to choose from printed menu, reads input, calls appropriate function */
void Pokedex::getChoice (char &choice)
{
	char buf = ' ', prompt[MAX_CHAR] {"Choose from menu: "};

	readInput (prompt, buf);
	buf = toupper(buf);
	choice = buf;

	switch (choice)
	{
		case 'A': read_list(); break;
		case 'D': display_list(); break;
		case 'S': search_type(); break;
		// Deactivated	case 'R': delete_pokemon(); break;
		case 'Q': ; break;
		default: resetView(); cout << "Not a menu option\n\n"; break;
	}
}

/* Delete pocket monster from list
DOES NOT WORK - Need to learn move semantics
void Pokedex::delete_pokemon()
{
	char prompt[MAX_CHAR]{"Enter number of corresponding index: "};
	int index (0), temp (num_pokemon);

	resetView();
	cout << "Choose pokemon to delete...\n";
	lineBreak();
	for (int i = 0; i < num_pokemon; ++i)
	{
		cout << i + 1 << ") " << list[i].display_name() << endl;
	}
	cout << "\n\n";
	readInput(prompt, index);
	while (((index - 1) < 0) && ((index - 1) > num_pokemon))
	{
		cout << "Invalid choice! Try again.\n\n";
		cout << "Choose pokemon to delete...\n";
		lineBreak();
		display_one();
		readInput(prompt, index);
	}

	--index;
	
	if (!index)
	{
		if (num_pokemon == 1)
		{
			--num_pokemon;
			cout << "\n\n---Delete from beginning---\n" << endl;
		}
		else
		{
			for (int i = 0; i < num_pokemon - 1; ++i)
			{
				list[i] = list[i + 1];
				cout << "\n\n---Delete from beginning---\n" << endl;
			}
		}
		--num_pokemon;
	}
	else if (index == num_pokemon)
	{
		cout << "\n\n---Delete from end---\n" << endl;
		--num_pokemon;
	}
	else
	{
		for (int i = index; i < num_pokemon - 1; ++i)
		{
			list[i] = list[i + 1];
			cout << "\n\n---Delete from middle---\n" << endl;
		}
		--num_pokemon;
	}
	//resetView();
	cout << "\n\nDeleted!\n\n";
}
 */
