#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include <iomanip>
#include <cctype>
#include <cstring>
#include <unistd.h>

using namespace std;

const int I_INIT (0);
const float F_INIT (0.0);
const int BUF_MAX (150);
const int MAX_CHAR (75);
const int IGNORE (1000);
const int WAIT (1000000);

void readInput (char [], int&); // overloaded
void readInput (char [], char []); // overloaded
void readInput (char*, char&); // overloaded
void setTimeout ();

#endif
