#include <iostream>
#include "pokedex.h"
#include "utils.h"
#include "pokemon.h"
#include "printFunc.h"
/******************************************************************************
# Author:           Aaron Willett
# Date:             11.15.2023
# Class:	    CS162 - 008
# Description:      Pokedex application! Save pokemon you find in the wild and
#                   record their information.
#******************************************************************************/

int main ()
{
	char choice {' '};
	intro();
	Pokedex p_list;
	do
	{
		menu();
		p_list.getChoice(choice);
	} while (choice != 'Q');

	endProg();
	return 0;
}
