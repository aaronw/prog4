#include "utils.h"
#include "pokemon.h"

/*	
 * data members:
 * char[] name, type, attack_desc, weakness
 * int numCandy
 */

/* constructor for pokemon class. Intializes data members to zero or null */
pokemon::pokemon()
{
	name = NULL;
	type = NULL;
	attack_desc = NULL;
	weakness = NULL;
	numCandy = 0;
}

/* deconstructor for dynamic memory for pokemon class */
pokemon::~pokemon()
{
	if (name)
		delete [] name;
	name = NULL;
	if (type)
		delete [] type;
	type = NULL;
	if (attack_desc)
		delete [] attack_desc;
	attack_desc = NULL;
	if (weakness)
		delete[] weakness;
	weakness = NULL;
}

/* prompts user for new pokemon entry */
void pokemon::add_p()
{
	char nPrompt[MAX_CHAR]{"Enter Pokemon's name: "},
	     tPrompt[MAX_CHAR]{"Enter Pokemon's type: "},
	     aPrompt[MAX_CHAR]{"Enter Pokemon's attacks and descriptions: "},
	     wPrompt[MAX_CHAR]{"Enter Pokemon's weakness: "},
	     cPrompt[MAX_CHAR]{"Enter number of rare candy to evolve pokemon: "},
	     buffer[BUF_MAX];
	int cBuf = 0;

	// size & name input
	readInput(nPrompt, buffer);
	name = new char [strlen(buffer) + 1];
	strcpy(name, buffer);
	// size & type input
	readInput(tPrompt, buffer);
	type = new char [strlen(buffer) + 1];
	strcpy(type, buffer);
	// size & attack input
	readInput(aPrompt, buffer);
	attack_desc = new char [strlen(buffer) + 1];
	strcpy(attack_desc, buffer);
	// size & weakness input
	readInput(wPrompt, buffer);
	weakness = new char [strlen(buffer) + 1];
	strcpy(weakness, buffer);
	// rare candy input
	readInput(cPrompt, cBuf);
	numCandy = cBuf;
}

/* prints data members for pokemon instance */
void pokemon::display()
{
	if (!name) return;
	cout << "Name: " << name << endl;
	cout << "Type: " << type << endl;
	cout << "Attack Description: " << attack_desc << endl;
	cout << "Weakness: " << weakness << endl;
	cout << "Rare Candy #: " << numCandy << endl << endl;
}

/* returns name data member */
char * pokemon::display_name()
{
	if (!name) return NULL;
	return name;
}

/* returns data member 'type' from pokemon object */
char * pokemon::get_type()
{
	if (!type) return NULL;
	return type;
}
