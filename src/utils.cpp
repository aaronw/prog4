#include "utils.h"
#include "printFunc.h"

/* Function: readInput (overloaded)
 * Desc: reads user input for int type, validates and passes value */
void readInput (char prompt[], int &input) // overloaded
{
	int buf (0);

	cout << prompt;
	cin >> buf;
	cin.ignore(IGNORE, '\n');

	while (!cin || buf < 0)
	{
		cout << "Invalid entry. Try again!\n";
		cin.clear();
		cin.ignore(IGNORE, '\n');
		cout << prompt;
		cin >> buf;
		cin.ignore(IGNORE, '\n');
	}
	input = buf;
}

/* Function: readInput (overloaded)
 * Desc: reads user input for c-string type, validates and passes value */
void readInput (char prompt[], char input[]) // overloaded
{
	char buf[BUF_MAX];
	
	cout << prompt;
	cin.getline(buf, BUF_MAX);

	while (!cin)
	{
		cout << "Entry has too many characters. Try again.\n";
		cin.clear();
		cin.ignore(IGNORE, '\n');
		cout << prompt;
		cin.getline(buf, BUF_MAX);
	}
	strcpy(input, buf);
}

/* Function: readInput (overloaded)
 * Desc: reads user input for char type, validates and passes value */
void readInput (char prompt[], char &input) // overloaded
{
	char buf = ' ';

	cout << prompt;
	cin >> buf;
	cin.ignore(IGNORE, '\n');

	while (!cin)
	{
		cout << "Invalid entry. Try again.\n";
		cin.clear();
		cin.ignore(IGNORE, '\n');
		cout << prompt;
		cin >> buf;
		cin.ignore(IGNORE, '\n');
	}
	input = buf;
}

/* Function: setTimeout
 * Desc: pauses processes for 1 second */
void setTimeout()
{
	int wait = WAIT;

	usleep(wait);
}

