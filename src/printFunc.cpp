#include "utils.h"
#include "printFunc.h"

/* resets view for user */
void resetView()
{
	int lines = I_INIT;

	while (lines < 50)
	{
		cout << "\n\n\n\n\n\n\n";
		++lines;
	}
}

/* line of tildas */
void lineBreak()
{
	cout << setfill('~') << setw(78) << ' ' << endl;
}

/* start program print */
void intro()
{
	resetView();
	lineBreak();
	lineBreak();
	cout << "||                                   ,'\\                                   ||\n"
	     << "||      _.----.        ____         ,'  _\\   ___    ___     ____           ||\n"
	     << "||  _,-'       `.     |    |  /`.   \\,-'    |   \\  /   |   |    \\  |`.     ||\n"
	     << "||  \\      __    \\    '-.  | /   `.  ___    |    \\/    |   '-.   \\ |  |    ||\n"
	     << "||   \\.    \\ \\   |  __  |  |/    ,','_  `.  |          | __  |    \\|  |    ||\n"
	     << "||     \\    \\/   /,' _`.|      ,' / / / /   |          ,' _`.|     |  |    ||\n"
	     << "||      \\     ,-'/  /   \\    ,'   | \\/ / ,`.|         /  /   \\  |     |    ||\n"
	     << "||       \\    \\ |   \\_/  |   `-.  \\    `'  /|  |    ||   \\_/  | |\\    |    ||\n"
	     << "||        \\    \\ \\      /       `-.`.___,-' |  |\\  /| \\      /  | |   |    ||\n"
	     << "||         \\    \\ `.__,'|  |`-._    `|      |__| \\/ |  `.__,'|  | |   |    ||\n"
	     << "||          \\_.-'       |__|    `-._ |              '-.|     '-.| |   |    ||\n"
	     << "||                                  `'                            '-._|    ||\n"
	     << setfill('~') << setw(78) << ' ' << setfill(' ') << '\n' << "Initializing...\n";
	lineBreak();
	setTimeout();
}

void menu () // menu
{
	lineBreak();
	cout << "Main Menu\n";
	lineBreak();

	cout << "\n(A)dd Pokémon\n";
	cout << "(D)isplay Pokémon\n";
	cout << "(S)earch Pokémon by type\n";
// Deactivated	cout << "(R)emove Pokémon by name\n";
	cout << "(Q)uit\n\n";
}


/* end program print */
void endProg()
{
	cout << "Pokedex powering down...\n\n\n";
	setTimeout();
	cout << "Goodbye!" << endl << endl;
}
